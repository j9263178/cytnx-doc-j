Bond
=======
A **Bond** is an object that represent the legs of a tensor. It carries informations such as direction, dimension and quantum numbers (if with Symmetry). 

There are in general two types of Bonds: **directional** and **undirectional** depending on whether the bond has direction (pointing inward to or outward from the tensor body) or not. The inward Bond is also defined as **Ket** type, and the outward Bond is defined as **Bra** type, which represent the *Braket* notation in the quantum mechanic: 

.. image:: image/bond.png
    :width: 400
    :align: center


Create a Bond
********************



Bond with direction
**********************


Bond with Symmetry
**********************


Multiple Symmetry
********************





.. toctree::


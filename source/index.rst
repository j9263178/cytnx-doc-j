.. Cytnx documentation master file, created by
   sphinx-quickstart on Sat Jun 20 22:23:26 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: Icon_small.png
    :width: 350    

Cytnx user guide and examples
=================================
    Cytnx is a library design for Quantum physics simulation using GPUs and CPUs.

.. toctree::
   :maxdepth: 1

   Intro.rst

.. toctree::
   :maxdepth: 3

   install.rst
   Guide.rst
   Examples.rst
   Perf_tune.rst

.. toctree::
    API Documentations <https://kaihsin.github.io/Cytnx/docs/html/index.html>   
    Github <https://github.com/kaihsin/Cytnx>   
    5-mins Blitz intro slide <https://drive.google.com/file/d/1vuc_fTbwkL5t52glzvJ0nNRLPZxj5en6/view?usp=sharing>

.. toctree::

    FAQ.rst    
    Developer.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
